#!/usr/bin/python3

from http import server
import threading

redirect_to='http://initial.invalid'

class Redirector(server.BaseHTTPRequestHandler):
	def do_GET(self):
		self.send_response(303)
		self.send_header('Location',redirect_to)
		self.end_headers()

def run():
	server_address = ('', 8000)
	httpd = server.HTTPServer(server_address, Redirector)
	httpd.serve_forever()

threading.Thread(target=lambda:run()).start()

while (True):
	redirect_to=input()
